from enum import Enum


class Sport(Enum):
    FOOTBALL = 0
    VOLLEYBALL = 1
    TENNIS = 2
    BASKET = 3


class MaxPlayers(Enum):
    SMALL = 5
    MEDIUM = 8
    LARGE = 11


class GroudType(Enum):
    RED = 0
    GRASS = 1


class Structure:
    def __init__(self, id: str, address: str, fields: list):
        self.id = id
        self.address = address
        self.fields = fields

    def getId(self):
        return self.id


class Field:
    def __init__(self, id: str, price: float, covered: bool, sport: Sport):
        self.id = id
        self.price = price
        self.covered = covered
        self.sport = sport

    def setSport(self, _sport: int):
        self.sport = Sport(_sport)


class FootBallField(Field):
    def __init__(self, id: str, price: float, covered: bool, maxPlayers: MaxPlayers):
        super().__init__(id, price, covered, Sport.FOOTBALL)
        self.maxPlayers = maxPlayers


class TennisField(Field):
    def __init__(self, id: str, price: float, covered: bool, groundType: GroudType):
        super().__init__(id, price, covered, Sport.FOOTBALL)
        self.groundType = groundType


class VolleyBallField(Field):
    def __init__(self, id: str, price: float, covered: bool):
        super().__init__(id, price, covered, Sport.VOLLEYBALL)


class BasketField(Field):
    def __init__(self, id: str, price: float, covered: bool):
        super().__init__(id, price, covered, Sport.BASKET)


if __name__ == '__main__':
    b = FootBallField('sss', 10.5, False, MaxPlayers(11))
    print(b.sport)
    print(b.covered)
    print(b.id)
    v = Volley('sss', 10.5, False, Sport.VOLLEYBALL)
    print(v)
