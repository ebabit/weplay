import string
import manager as ma


# generica
def check_login(kind, db) -> object:
    _check, user_founded = False, False,
    while not _check:
        if not user_founded:
            username = input('Insert username :')
            user_founded = db.find(username, kind) != False
            continue

        password = input('Insert password :')
        _check = password == db.find(username, kind).getPassword()

    return db.find(username, kind)


def checkDigits(_str, _len):
    return all([len(_str) == _len, [i in string.digits for i in _str]])

