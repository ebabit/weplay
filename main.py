import tools
import manager as ma
import person


class State:
    def __init__(self, title: str, labels: tuple, state_values: tuple):
        self.title = title
        self.labels = labels
        self.state_values = state_values

    def dictBuilder(self) -> dict:
        return {str(i): _value for i, _value in enumerate(self.state_values, start=1)}

    def __str__(self) -> str:
        return f'{self.title} \n'+"\n".join(f"{i} -> {label}" for i, label in enumerate(self.labels, start=1))


def generic_loop(description: str, operation_dict: dict) -> int:

    print(description)
    while True:
        _input = input('Select operation: ')
        if _input not in operation_dict:
            print(f"{_input} is not allowed operation")
            continue
        return operation_dict.get(_input)


def unpack(_state: State) -> tuple: return str(_state), _state.dictBuilder()


main_menu = State("Select user's type", ("Admin", 'User'), (1, 2))
owner_sign_log = State("Choose your operation", ("Sign in", 'Login', "Back"), (3, 4, 0))
user_sign_log = State("Choose your operation", ("Sign in", 'Login', "Back"), (3, 4, 0))

database = ma.dbs_weplay

if __name__ == '__main__':
    state = 0
    while True:

        while not state:
            state = generic_loop(*unpack(main_menu))

        while state == 1:
            state = generic_loop(*unpack(owner_sign_log))

            if state == 3:
                person.Owner.shell_newOwner(database)
                state = 1

            if state == 4:
                owner = tools.check_login('owners', database)
                print(owner)
                state = 0

        while state == 2:
            state = generic_loop(*unpack(user_sign_log))

            if state == 3:
                database.update('users', person.User.shell_newUser(database))
                state = 2

            if state == 4:
                user = tools.check_login('users', database)
                print(user)
                state = 0


























































