import datetime
import string as st
import random as ra
import tools
import manager as ma


class Person:

    def __init__(self, id: str, name: str, sname: str, password: str, birth: str, email: str):
        self.id = id
        self.name = name
        self.sname = sname
        self.password = password
        self.birth = birth
        self.email = email


    def getId(self):
        return self.id

    def getPassword(self):
        return self.password

    def __str__(self):
        self.credentials = [self.id, self.name, self.sname, self.birth, self.email]
        self.credentials_labels = ['Username', 'Name', 'Second name', 'Birth', 'Email']
        return "\n".join(f'{label}: {value}' for label, value in zip(self.credentials_labels, self.credentials))


class Owner(Person):

    def __init__(self, id: str, name: str, sname: str, password: str, birth: str, email: str, taxcode: str,
                 structure: list):
        super().__init__(id, name, sname, password, birth, email)
        self.taxcode = taxcode
        self.structure = structure

    @classmethod
    def shell_newOwner(cls, dbs):
        print('da implementare')
        pass


class User(Person):

    def __init__(self, id: str, name: str, sname: str, password: str, birth: str, email: str, enrollment: datetime,
                 creditcards: list):

        self.enrollment = enrollment
        self.creditcards = creditcards

        super().__init__(id, name, sname, password, birth, email)









    @classmethod
    def shell_newUser(cls, dbs: object):
        new = cls.__new__(cls)

        username_check = False
        while not username_check:
            id_input = input('Username :')
            username_check = dbs.find(id_input, 'users') == False


        new.id = id_input
        new.name = input('First name : ')
        new.sname = input('Second name : ')
        new.email = input('Email : ')

        password_check = False
        while not password_check:
            p_1 = input('Insert password :')
            p_2 = input('Insert password for check:')
            password_check = p_1 == p_2

        new.password = p_1
        new.enrollment = datetime.datetime.now()
        new.creditcards = []
        return new


class CreditCard:

    def __init__(self, owner: str, number: str, cvc: str, expiry: str):
        self.owner = owner
        self.number = number
        self.cvc = cvc
        self.expiry = expiry

    def checkValidity(self):
        pass

    @classmethod
    def shell_newCreditCard(cls):
        new = cls.__new__(cls)
        new.owner = input('Owner : ')

        is_valid_number = False
        while not is_valid_number:
            number = input('Credit number :')
            is_valid_number = tools.checkDigits(number, 16)
        new.number = number

        is_valid_cvc = False
        while not is_valid_cvc:
            cvc = input('Insert cvc : ')
            is_valid_cvc = tools.checkDigits(cvc, 3)
        new.cvc = cvc

        # salvare in formato data e controllare che la data di scadenza non sia antecedente alla registrazione
        new.expiry = input('Insert expiry date : ')

        return new


# if __name__ == '__main__':
#     ca = Owner('enrico94', 'Enrico', 'Esposito', '12345', '01/01/94', 'e@gmail.com', '01/01/94', [])
#     ba = User('id', 'name', 'sname', 'password', 'birth', 'email', 'enrollment', [])
