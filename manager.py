import shelve


class DbsManager:
    def __init__(self, owners: str, users: str, structures: str):
        self.owners = owners
        self.users = users
        self.structures = structures

        self.dbs_dict = {

            'owners': self.owners,
            'users': self.users,
            'structures': self.structures
        }

    def find(self, key: str, kind: str) -> bool or object:
        with shelve.open(self.dbs_dict[kind]) as self.db:
            if key in [_key for _key in self.db.keys()]:
                return self.db[key]
            return False

    def update(self, kind: str, data_input: object):
        with shelve.open(self.dbs_dict[kind]) as self.db:
            self.db[data_input.getId()] = data_input


# app's database
dbs_weplay = DbsManager('db_owners', 'db_users', 'db_structures')